# Final Frontend Project

## Authors

* Alina Paukova
* Max Kureykin

## Components

* Authentication (Registration/Login)
* Board
* Task
* Profile
* Statistics

## Sketch design

### Authentication (Registration/Login)

![](design/IMG_2490.JPG)

### Board and Statistics

![](design/IMG_2489.JPG)

### Task

![](design/IMG_2488.JPG)

### Profile

![](design/IMG_2491.JPG)