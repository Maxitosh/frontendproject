from django.urls import path
from rest_framework.routers import DefaultRouter

# Создаем router и регистрируем ViewSet
from api.views import TaskListApi, UpdateProfileView
from api.viewsets import TaskViewSet

router = DefaultRouter()
router.register(r'task', TaskViewSet)
urlpatterns = router.urls
urlpatterns += [
    path('tasks/', TaskListApi.as_view(), name="tasks-user"),
    path('update_profile/<int:pk>/', UpdateProfileView.as_view(), name='auth_update_profile')
]
