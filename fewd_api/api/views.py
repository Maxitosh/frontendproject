from django.shortcuts import render

# Create your views here.
from rest_framework import permissions, status, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Task, User
from api.serializers import TaskSerializer, UpdateUserSerializer


class TaskListApi(APIView):
    """
    List all tasks, or create a new task.
    """
    permission_classes = [permissions.IsAuthenticated]

    # filter tasks by requesting user, measure of security
    def get(self, request, format=None):
        tasks = Task.objects.filter(author=request.user)
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data)


class UpdateProfileView(generics.UpdateAPIView):

    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = UpdateUserSerializer
