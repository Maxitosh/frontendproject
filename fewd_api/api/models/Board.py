from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Board(models.Model):
    id = models.AutoField(primary_key=True)
    author = models.ForeignKey('api.User', verbose_name="Пользователь", on_delete=models.CASCADE)
    name = models.TextField()
    pub_date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['pub_date']

    def __str__(self):
        return f"{self.author}|{self.name}"
