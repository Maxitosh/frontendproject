from django.db import models
from django.db.models import Manager
from django.utils import timezone


class Task(models.Model):
    id = models.AutoField(primary_key=True)
    # board = models.ForeignKey(Board, verbose_name="Доска", on_delete=models.CASCADE)
    author = models.ForeignKey('api.User', verbose_name="Пользователь", on_delete=models.CASCADE)
    name = models.TextField(default="Some task")
    description = models.TextField(default="Some description")
    pub_date = models.DateTimeField(default=timezone.now)
    LOW = 'LOW'
    NORMAL = 'NORMAL'
    HIGH = 'HIGH'
    PRIORITY_CHOICES = [
        (LOW, 'LOW'),
        (NORMAL, 'NORMAL'),
        (HIGH, 'HIGH'),
    ]
    priority = models.CharField(
        max_length=6,
        choices=PRIORITY_CHOICES,
        default=NORMAL,
    )
    TODO = 'TODO'
    PROGRESS = 'PROGRESS'
    VERIFY = 'VERIFY'
    DONE = 'DONE'
    STATUSES = [
        (TODO, 'TODO'),
        (PROGRESS, 'PROGRESS'),
        (VERIFY, 'VERIFY'),
        (DONE, 'DONE'),
    ]
    status = models.CharField(
        max_length=8,
        choices=STATUSES,
        default=TODO,
    )
    estimate = models.IntegerField(default=0)

    objects = Manager()

    class Meta:
        ordering = ['pub_date']

    def __str__(self):
        return f""
