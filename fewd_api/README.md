# API
## Overview

API for FeWD project


## Technologies

* Django
* DRF
* Djoser
* JWT
* Swagger


## Basic features - Endpoints

### User signup
#### Endpoint

> /auth/users/ - Method POST

#### Overview

Registration of new user

#### Parameters

| Parameters | Required | Type |
| -------- | -------- | -------- | 
| username     | :heavy_check_mark:     | Text     |
| password     | :heavy_check_mark:     | Text     |


#### CURL

```console
curl --location --request POST 'http://127.0.0.1:8000/auth/users/' \
--form 'username="testuser2"' \
--form 'password="testpass2"'
```

#### Response example

```json
{
    "email": "",
    "username": "testuser2",
    "id": 2
}
```

### User login 
#### Endpoint

> /auth/jwt/create/ - Method POST
#### Overview

Login of user is a JWT token generation.

#### Parameters

| Parameters | Required | Type |
| -------- | -------- | -------- | 
| username     | :heavy_check_mark:     | Text     |
| password     | :heavy_check_mark:     | Text     |


#### CURL

```console
curl --location --request POST 'http://127.0.0.1:8000/auth/jwt/create/' \
--form 'username="testuser2"' \
--form 'password="testpass2"'
```

#### Response example

```json
{
    "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxODU2ODYzOSwianRpIjoiM2IyY2NhMTYyM2ZkNDA1MWEzMzkyYjJhZDJjMTc3YWUiLCJ1c2VyX2lkIjoxfQ.aEe01dcpVo9Dysx-jnZPNY6bX1hRsoyxzmnKM23ejo0",
    "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE4NDgzMTM5LCJqdGkiOiJlOGNmNDk2ODU4YjA0MWE0YmZkNWMyYjA0NmEyNjkzZiIsInVzZXJfaWQiOjF9.bDjV8Nngx4PGJZdZOVA_8rfE25IbVKvo5QoX-Kw8j9g"
}
```
