from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions

apipatterns = [
    url(r"^", include("api.urls")),
]

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="fewd_api API",
        default_version="v1",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    url(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    url(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
]
urlpatterns += [
    url(r"^$", schema_view),
    path("admin/", admin.site.urls),
    url(r"^api/v1/", include((apipatterns, "api"))),
    url(r"^auth/", include("djoser.urls")),
    url(r"^auth/", include("djoser.urls.jwt")),
]
