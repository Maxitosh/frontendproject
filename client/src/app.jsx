import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Profile from './pages/Profile'
import NewTask from './pages/NewTask'
import TaskPage from './pages/TaskPage'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
        <Route exact path="/newTask">
          <NewTask />
        </Route>
        <Route exact path="/task/:id">
          <TaskPage />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App
