import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import './Task.css'
import userService from '../../services/user.service'

export default function Task({ taskTitle, priority, taskId, status, column }) {
  const history = useHistory()
  const [error, setError] = useState()

  const goToTask = () => {
    history.push(`/task/${taskId}`)
  }

  const statuses = ['TODO', 'PROGRESS', 'VERIFY', 'DONE']

  const left = async () => {
    try {
      const newStatus = statuses[statuses.indexOf(status) - 1]
      await userService.changeTaskStatus(taskId, newStatus)
      history.replace(`/task/`)
      history.replace('/')
    } catch {
      setError('Some error')
    }
  }

  const right = async () => {
    try {
      const newStatus = statuses[statuses.indexOf(status) + 1]
      await userService.changeTaskStatus(taskId, newStatus)
      history.replace(`/task/`)
      history.replace('/')
    } catch {
      setError('Some error')
    }
  }

  return (
    <div className="task">
      {column !== 'TO DO' && (
        <div className="arrow-left" onClick={left} role="button" aria-label="Move to left" tabIndex={0} />
      )}
      <div className="taskInfo">
        <h2 className="taskTitle" onClick={goToTask}>
          {taskTitle}
        </h2>
        <h3 className="taskPriority">priority: {priority}</h3>
      </div>
      {column !== 'DONE' && (
        <div className="arrow-right" onClick={right} role="button" aria-label="Move to right" tabIndex={0} />
      )}
    </div>
  )
}
