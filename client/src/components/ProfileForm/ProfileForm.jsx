import React from 'react'
import Form from '../ui/Form/Form'
import Input from '../ui/Input/Input'
import './ProfileForm.css'
import image from './static/image.jpg'

function ProfileForm({ onSubmitChangeProfile, onSubmitChangePassword, error1, error2, title, profile }) {
  return (
    <div>
      <h2 className="profileTitle">{title}</h2>
      <img src={profile.avatar_url === '' ? image : profile.avatar_url} alt="avatar" />
      <Form
        onSubmit={onSubmitChangeProfile}
        defaultValues={{
          username: profile.username,
          firstName: profile.first_name,
          lastName: profile.last_name,
          email: profile.email,
          avatarUrl: profile.avatar_url
        }}
      >
        <Input name="username" label="Username" rules={{ required: { message: 'Username is required', value: true } }} />
        <Input name="firstName" label="First Name" rules={{ required: { message: 'First Name is required', value: true } }} />
        <Input name="lastName" label="Last Name" rules={{ required: { message: 'Last Name is required', value: true } }} />
        <Input name="email" label="Email" rules={{ required: { message: 'Email is required', value: true } }} />
        <Input name="avatarUrl" label="Avatar URL" />
        {error1 && <div>{error1}</div>}
        <input type="submit" value="Submit" />
      </Form>
      <h2>Change password</h2>
      <Form onSubmit={onSubmitChangePassword}>
        <Input name="currentPassword" label="Current Password" />
        <Input name="newPassword" label="New Password" />
        {error2 && <div>{error2}</div>}
        <input type="submit" value="Submit" />
      </Form>
    </div>
  )
}

export default ProfileForm
