import React from 'react'
import Form from '../ui/Form/Form'
import Input from '../ui/Input/Input'
import InputOptions from '../ui/InputOptions/InputOptions'
import './TaskForm.css'

function TaskForm({ onSubmit, title, task }) {
  return (
    <div>
      <h2>{title}</h2>
      <h3>Publication date: {task.pub_date.substring(0, 10)}</h3>
      <Form
        onSubmit={onSubmit}
        defaultValues={{
          name: task.name,
          description: task.description,
          priority: task.priority,
          estimation: task.estimate,
          status: task.status
        }}
      >
        <Input name="name" label="Name" />
        <Input name="description" label="Description" />
        <Input name="estimation" label="Estimation" />
        <InputOptions
          name="priority"
          label="Priority"
          options={['HIGH', 'NORMAL', 'LOW']}
          rules={{ required: { message: 'Priority is required', value: true } }}
        />
        <InputOptions
          name="status"
          label="Status"
          options={['TODO', 'PROGRESS', 'VERIFY', 'DONE']}
          rules={{ required: { message: 'Status is required', value: true } }}
        />
        <Input name="files" label="File URL" />
        <input type="submit" value="Submit" />
      </Form>
    </div>
  )
}

export default TaskForm
