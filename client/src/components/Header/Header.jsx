import React from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import authService from '../../services/auth.service'
import './Header.css'
import homeImage from './static/home.png'

function Header() {
  const history = useHistory()
  const handleLogout = () => {
    authService.logout()
    history.replace('/login')
  }

  return (
    <header>
      {!authService.isAuthorized() ? (
        <>
          <NavLink to="/login" className="navlinkAuth">
            Login
          </NavLink>
          <NavLink to="/register" className="navlinkAuth">
            Register
          </NavLink>
        </>
      ) : (
        <>
          <NavLink to="/" className="navlink" >
            <img className="homeImage" src={homeImage} alt="home" />
          </NavLink>
          <NavLink to="/profile" className="navlink">
            Profile
          </NavLink>
          <NavLink to="/login" className="navlink" onClick={handleLogout}>
            Logout
          </NavLink>
        </>
      )}
    </header>
  )
}

export default Header
