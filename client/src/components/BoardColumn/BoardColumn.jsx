import React from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import Task from '../Task/Task'
import './BoardColumn.css'

export default function BoardColumn({ title, tasks }) {

  return (
    <div className="column">
      <h2 className="columnTitle">{title}</h2>
      {tasks.map((task) => (
        <Task taskTitle={task.name} priority={task.priority} taskId={task.id} status={task.status} column={title} />
      ))}
      {title === 'TO DO' && (
        <NavLink to="/newTask" className="navlinkNewTask">
          <div className="addTask">
            +
          </div>
        </NavLink>
      )}
    </div>
  )
}
