import React from 'react'
import Form from '../ui/Form/Form'
import Input from '../ui/Input/Input'
import InputOptions from '../ui/InputOptions/InputOptions'

function NewTaskForm({ onSubmit, title, error }) {
  return (
    <div className="loginForm">
      <h2>{title}</h2>
      <Form onSubmit={onSubmit} defaultValues={{ name: '', priority: 'NORMAL' }}>
        <Input name="name" label="Name" rules={{ required: { message: 'Name is required', value: true } }} />
        <InputOptions
          name="priority"
          label="Priority"
          options={['HIGH', 'NORMAL', 'LOW']}
          rules={{ required: { message: 'Priority is required', value: true } }}
        />
        {error && <p>{error}</p>}
        <button type="submit">Submit</button>
      </Form>
    </div>
  )
}

export default NewTaskForm
