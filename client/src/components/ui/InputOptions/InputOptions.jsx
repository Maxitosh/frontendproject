import React from 'react'
import { useController } from 'react-hook-form'
import './InputOptions.css'

/**
 * @param label
 * @param name
 * @param rules
 * @param rest
 * @returns {JSX.Element}
 * @constructor
 */
function InputOptions({ label, name, rules, options, ...rest }) {
  const { field, fieldState } = useController({ name, rules })

  return (
    <label className="priorityLabel">
      {label}
      <select className="prioritySelect" name="priority" {...rest} {...field}>
        {options.map((option) => (
          <option value={option}>{option}</option>
        ))}
      </select>
      {fieldState.error && <div>{fieldState.error.message}</div>}
    </label>
  )
}

export default InputOptions
