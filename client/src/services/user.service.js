import axios from 'axios'
import authService from './auth.service'

class UserService {
  API_ENDPOINT = 'http://localhost:8000'

  constructor(authService) {
    this.authService = authService
  }

  async getTasks() {
    const response = await axios.get(`${this.API_ENDPOINT}/api/v1/tasks/`, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async newTask(name, priority) {
    const bodyFormData = new FormData()
    bodyFormData.append('name', name)
    bodyFormData.append('priority', priority)

    const response = await axios.post(`${this.API_ENDPOINT}/api/v1/task/`, bodyFormData, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async taskInfo(id) {
    const response = await axios.get(`${this.API_ENDPOINT}/api/v1/task/${id}`, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async changeTaskInfo(id, name, description, estimation, priority, status) {
    const bodyFormData = new FormData()
    bodyFormData.append('name', name)
    bodyFormData.append('description', description)
    bodyFormData.append('estimate', estimation)
    bodyFormData.append('priority', priority)
    bodyFormData.append('status', status)
    const response = await axios.put(`${this.API_ENDPOINT}/api/v1/task/${id}/`, bodyFormData, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async changeTaskStatus(id, status) {
    const bodyFormData = new FormData()
    bodyFormData.append('status', status)
    const response = await axios.put(`${this.API_ENDPOINT}/api/v1/task/${id}/`, bodyFormData, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async deleteTask(id) {
    const response = await axios.delete(`${this.API_ENDPOINT}/api/v1/task/${id}/`, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async profileInfo() {
    const response = await axios.get(`${this.API_ENDPOINT}/auth/users/me/`, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async changeProfileInfo(id, username, firstName, lastName, email, avatarUrl) {
    const bodyFormData = new FormData()
    bodyFormData.append('username', username)
    bodyFormData.append('first_name', firstName)
    bodyFormData.append('last_name', lastName)
    bodyFormData.append('email', email)
    bodyFormData.append('avatar_url', avatarUrl)
    const response = await axios.put(`${this.API_ENDPOINT}/api/v1/update_profile/${id}/`, bodyFormData, {
      headers: {
        Authorization: `Bearer ${authService.TOKEN}`
      }
    })
    return response.data
  }

  async changeProfilePassword(currentPassword, newPassword) {
    const response = await axios.post(
      `${this.API_ENDPOINT}/auth/users/set_password/`,
      {
        new_password: newPassword,
        current_password: currentPassword
      },
      {
        headers: {
          Authorization: `Bearer ${authService.TOKEN}`
        }
      }
    )
    return response.data
  }
}

const userService = new UserService(authService)

export default userService
