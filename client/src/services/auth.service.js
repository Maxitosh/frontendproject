import axios from 'axios'

class AuthService {
  API_ENDPOINT = 'http://127.0.0.1:8000'

  TOKEN

  registered

  isAuthorized() {
    return !!this.TOKEN
  }

  isRegistered() {
    return this.registered
  }

  async register(login, password) {
    try {
      const bodyFormData = new FormData()
      bodyFormData.append('username', login)
      bodyFormData.append('password', password)
      const response = await axios.post(`${this.API_ENDPOINT}/auth/users/`, bodyFormData)

      if (response.status === 201) {
        this.registered = true
      }
    } catch (e) {
      if (e.response) {
        throw new Error(e.response.data.message)
      }
      throw e
    }
  }

  async login(login, password) {
    try {
      const bodyFormData = new FormData()
      bodyFormData.append('username', login)
      bodyFormData.append('password', password)
      const response = await axios.post(`${this.API_ENDPOINT}/auth/jwt/create/`, bodyFormData)
      if (response.data?.access) {
        this.TOKEN = response.data.access
      }
    } catch (e) {
      if (e.response) {
        this.ERROR = e.response.data.message
        throw new Error(e.response.data.message)
      }
      throw e
    }
    return this.TOKEN
  }

  logout() {
    this.TOKEN = undefined
  }
}

const authService = new AuthService()

export default authService
