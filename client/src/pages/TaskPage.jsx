import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { useHistory } from 'react-router-dom'
import Layout from '../components/Layout/Layout'
import authService from '../services/auth.service'
import userService from '../services/user.service'
import TaskForm from '../components/TaskForm/TaskForm'
import './TaskPage.css'

function TaskPage() {
  const history = useHistory()
  const { id } = useParams()
  const [task, setTask] = useState(undefined)
  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(false)

  const onSubmit = async ({ name, description, estimation, priority, status }) => {
    try {
      await userService.changeTaskInfo(task.id, name, description, estimation, priority, status)
      history.push('/')
    } catch {
      setError('Some error')
    }
  }

  const deleteTask = async () => {
    try {
      await userService.deleteTask(task.id)
      history.push('/')
    } catch {
      setError('Some error')
    }
  }

  useEffect(() => {
    if (authService.isAuthorized()) {
      setLoading(true)
      userService
        .taskInfo(id)
        .then((data) => setTask(data))
        .catch((error) => {
          setError('Some error happened:')
        })
        .finally(() => {
          setLoading(false)
        })
    } else {
      setTask(null)
      history.replace('/login')
    }
  }, [authService.isAuthorized()])

  return (
    <Layout>
      {!!task && <TaskForm onSubmit={onSubmit} title="Task Info" error={error} task={task} />}
      {!!task && <input className="DeleteButton" type="submit" value="Delete" onClick={deleteTask} />}
    </Layout>
  )
}

export default TaskPage
