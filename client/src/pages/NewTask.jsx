import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import authService from '../services/auth.service'
import Layout from '../components/Layout/Layout'
import userService from '../services/user.service'
import NewTaskForm from '../components/NewTaskForm/NewTaskForm'

export default function NewTask() {
  const history = useHistory()
  const [error, setError] = useState()

  const onSubmit = async ({ name, priority }) => {
    try {
      await userService.newTask(name, priority)
      history.replace('/')
    } catch {
      setError('Some error')
    }
  }

  if (authService.isAuthorized()) {
    return (
      <Layout>
        <NewTaskForm onSubmit={onSubmit} title="New Task" error={error} />
      </Layout>
    )
  }
  history.replace('/login')
  return null
}
