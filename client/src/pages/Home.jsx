import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import BoardColumn from '../components/BoardColumn/BoardColumn'
import Layout from '../components/Layout/Layout'
import authService from '../services/auth.service'
import userService from '../services/user.service'
import './Home.css'

function Home() {
  const [tasks, setTasks] = useState([])
  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(false)
  const history = useHistory()

  useEffect(() => {
    if (authService.isAuthorized()) {
      setLoading(true)
      userService
        .getTasks()
        .then((data) => {
          setTasks(data)
        })
        .catch((error) => {
          setError('Some error happened:')
        })
        .finally(() => {
          setLoading(false)
        })
    } else {
      // TODO reset users state
      setTasks([])
      history.replace('/login')
    }
  }, [authService.isAuthorized()])

  if (authService.isAuthorized()) {
    const tasks1 = []
    const tasks2 = []
    const tasks3 = []
    const tasks4 = []
    for (let i = 0; i < tasks.length; i += 1) {
      if (tasks[i].status === 'TODO') {
        tasks1.push(tasks[i])
      }
      if (tasks[i].status === 'PROGRESS') {
        tasks2.push(tasks[i])
      }
      if (tasks[i].status === 'VERIFY') {
        tasks3.push(tasks[i])
      }
      if (tasks[i].status === 'DONE') {
        tasks4.push(tasks[i])
      }
    }
    return (
      <Layout>
        {tasks.description}
        {error && <div>{error}</div>}
        <div className="row">
          <BoardColumn title="TO DO" tasks={tasks1} />
          <BoardColumn title="IN PROGRESS" tasks={tasks2} />
          <BoardColumn title="VERIFY" tasks={tasks3} />
          <BoardColumn title="DONE" tasks={tasks4} />
        </div>
      </Layout>
    )
  }
  history.replace('/login')
  return null
}

export default Home
