import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import authService from '../services/auth.service'
import Layout from '../components/Layout/Layout'
import ProfileForm from '../components/ProfileForm/ProfileForm'
import userService from '../services/user.service'

export default function Profile() {
  const history = useHistory()
  const [error1, setError1] = useState()
  const [error2, setError2] = useState()
  const [profile, setProfile] = useState(undefined)

  const onSubmitChangeProfile = async ({ username, firstName, lastName, email, avatarUrl }) => {
    try {
      await userService.changeProfileInfo(profile.id, username, firstName, lastName, email, avatarUrl)
      history.push('/')
    } catch {
      setError1('Please, fill all fields')
    }
  }
  const onSubmitChangePassword = async ({ currentPassword, newPassword }) => {
    try {
      await userService.changeProfilePassword(currentPassword, newPassword)
      history.push('/')
    } catch {
      setError2('Incorrect password')
    }
  }

  useEffect(() => {
    if (authService.isAuthorized()) {
      userService
        .profileInfo()
        .then((data) => setProfile(data))
        .catch((error) => {
          setError1(error)
        })
        .finally(() => {})
    } else {
      setProfile(null)
      history.replace('/login')
    }
  }, [authService.isAuthorized()])

  if (authService.isAuthorized()) {
    return (
      <Layout>
        {!!profile && (
          <ProfileForm
            onSubmitChangeProfile={onSubmitChangeProfile}
            onSubmitChangePassword={onSubmitChangePassword}
            title="Change Profile Info"
            error1={error1}
            error2={error2}
            profile={profile}
          />
        )}
      </Layout>
    )
  }
  history.replace('/login')
  return null
}
