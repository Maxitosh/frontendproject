import './App.css';
import BaseRouter from './BaseRouter';
import Header from './components/Header/Header';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <BaseRouter></BaseRouter>
    </div>
  );
}

export default App;
