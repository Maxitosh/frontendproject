import React from 'react';
import './Header.css'
import { useHistory } from "react-router-dom";

export default function Header() {

    return (
        <header className="HomePage-header">
        <div className="HomePageDiv">
          <div className="Header" >
            <h3>Home</h3>
          </div>
        </div>
      </header>
    );
}