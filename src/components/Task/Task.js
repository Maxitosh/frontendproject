import React, {useState} from 'react'
import './Task.css'

export default function Task() {
    const [title, setTitle] = useState('Task1')
    return (
        <div className="task">
            <h1>{title}</h1>
            <form>
                <label>
                    <p>Description</p>
                    <input type="text"/>
                </label>
                <label>
                    <p>Estimation</p>
                    <input type="text"/>
                </label>
                <label>
                    <p>Deadline</p>
                    <input type="date"/>
                </label>
                <label>
                    <p>Files</p>
                    <input type="file"/>
                </label>
                <div>
                    <button type="submit">Save</button>
                </div>
            </form>
        </div>
    )
}