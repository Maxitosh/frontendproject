import React, { useState } from 'react'
import Header from '../Header/Header'
import './Profile.css'
import image from './static/image.png'

export default function Profile() {
    return(
      <div className="profile-wrapper"> 
          
          <form className="profile-form">
            <h1 className="profile">PROFILE</h1>
            <img src={image}></img>

            <label for="avatar"> Avatar url </label>
            <input type="text" />

            <label for="name"> Full name </label>
            <input type="text" />

            <label for="email"> Email </label>
            <input type="text" />

            <label for="oldpass"> Old password </label>
            <input type="text" />

            <label for="newpass"> New password </label>
            <input type="text" />

            <input type="submit" value="Submit"/>

          </form>
        </div>
    )
  }