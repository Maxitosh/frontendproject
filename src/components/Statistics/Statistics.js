import React, {useState} from 'react'
import './Statistics.css'
import image from "./static/clock.png"

export default function Statistics() {
    const [title, setTitle] = useState('Statistics')
    return (
        <div className="statistics">
            <div className="tasks-by-column">
                <span>2</span><span>1</span><span>0</span><span>1</span>
            </div>
            <div className="time">
                <img src={image}></img>
            </div>
        </div>
    )
}