import React from 'react';
import { Switch, Route, BrowserRouter, withRouter } from 'react-router-dom';
import Authentication from './components/Authentication/Authentication';
import Board from './components/Board/Board';
import Profile from './components/Profile/Profile';


export default function BaseRouter() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
        <Route
            exact
            path={'/'}
            component={Board}
          />
          <Route
            exact
            path={'/login'}
            component={Authentication}
          />
          <Route
            exact
            path={'/home'}
            component={Board}
          />
          <Route
            exact
            path={'/profile'}
            component={Profile}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
